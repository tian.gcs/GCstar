#!/usr/bin/perl
use strict;
use Getopt::Long;

sub usage
{
    print "Usage : $0 [-o outfile|-i] file.gcs\n";
    print " Converts a file created with GCstar 1.2.0 (or above)\n to a file readable by previous versions.\n\n";
    print "  -o specifies a file where the converted version should be written\n";
    print "  -i is used to do the modifications directly in file.gcs\n   (file.gcs~ is created as a backup)\n";
    print "\n  Without -o or -i option, modified file id displayed on standard output\n";
    exit;
}

if ($ARGV[0] eq '-o')
{
    open OUT, '>'.$ARGV[1];
    select(OUT);
    splice @ARGV, 0, 2;
}
elsif ($ARGV[0] eq '-i')
{
    $^I = '~';
    shift @ARGV;
}
usage if ($#ARGV > 1) || ($ARGV[0] =~ /^-/);
while (<>)
{
    s|<item$|<item>|;
    s|^(\s*)([a-zA-Z0-9]*)="(.*?)"|$1<$2>$3</$2>|;
    s|^\s*>\n||;
    print;
}
